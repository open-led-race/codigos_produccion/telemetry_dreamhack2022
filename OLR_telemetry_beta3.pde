/*
 * ____                     _      ______ _____    _____
  / __ \                   | |    |  ____|  __ \  |  __ \
 | |  | |_ __   ___ _ __   | |    | |__  | |  | | | |__) |__ _  ___ ___
 | |  | | '_ \ / _ \ '_ \  | |    |  __| | |  | | |  _  // _` |/ __/ _ \
 | |__| | |_) |  __/ | | | | |____| |____| |__| | | | \ \ (_| | (_|  __/
  \____/| .__/ \___|_| |_| |______|______|_____/  |_|  \_\__,_|\___\___|
        | |
        |_|
        
 Open LED Race Telemetry BETA 3

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 By G.Barbarov - OLR TEAM   for DreamHack Valencia 2022 
    
 
 Serial protocol doc   
 https://gitlab.com/open-led-race/olr-arduino/-/blob/lab/doc/OLRN_Protocol_Serial.pdf
*/

import processing.serial.*;

int TOT_LAPS =2;
int LEN_TRACK=25;// meters of LED strip of main track

int TOT_DIST = TOT_LAPS*LEN_TRACK;
String stext[]={"                           1               2             3               Ready for Open LED Race ?                       ",
                "                  GO !!!                  GO !!!                  Vamoooossss !!!                                        ",
                "                   !!!                                                                                                   ",                
               };
int CSCRL_R=0;
int CSCRL_G=200;
int CSCRL_B=255;

String scrl_text="";
int lscrl=42;//scrl_text.length();

//String name[]={"Red","Green","Blue","White","Driver 5","Driver 6"};  
String name[]={"MULTIVERSO","TIERRA MEDIA","ESCUELA DE MAGIA","IMPERIO","Driver 5","Driver 6"};  
String nom_status[]={"Wait","Run","In Pit","Out Pit","Charge"};  
String nom_pos[]={"---","1","2","3","4"};  

int  BOX_DY=40;
int  BOX_DX=1080;//1920;
int DY=50;
int DX=0; 
int DX_SCRL=10;
int TEXT_POS_X=0;
int TEXT_NAME_X=120;//200;
int TEXT_LAP_X=370;//700;
int TEXT_TLAP_X=480;//900;
int TEXT_TLAP_BEST_X=600;//1200;
int TEXT_DIST_X=500;//1000;
int TEXT_STAT_X=920;//1700;
int TEXT_BAT_X=750;

boolean clicked = false;



Serial SPort;
int SCount = 0;
String bstr="";
String bstr2="";
PFont f;
PFont f2;
int scrl=0;
int lap1=0;
int lap2=0;
long time=millis();
long itime=millis();
int list_pos[]={1,2,3,4};

int status_race=0;

int ncar1=-1;
int ncar2=-1;
int track1=1;
int track2=1;


int car_col_r[]={255,0,153,255,0,255};
int car_col_g[]={0,255,0,255,255,0};
int car_col_b[]={0,0,255,255,255,255};

int lap[]={0,0,0,0,0,0};
int status[]={0,0,0,0,0,0};
float dist[]={0,0,0,0};
float tlap[]={0,0,0,0,0,0};
float tlap_best[]={0,0,0,0,0,0};  
float bat[]={100,100,100,100,100,100}; 
PImage logo1;
PImage logo2;

  
  
void setup() {
  size(1080/*1920*/, 320,P3D);
  background(0);

  //printArray(PFont.list());
  f = createFont("MODE7GX3.TTF", 28); 
  f2 = createFont("advanced_led_board-7.ttf", 32);
  //f2 = createFont("MODE7GX0.TTF", 38);

  logo1 = loadImage("img/logo_event.png");
  logo2 = loadImage("img/logo2.png");
  loadTextScroll("                                       PULSAR EL BOTON TODOS LOS JUGADORES SIMULTANEAMENTE PARA INICIAR UNA NUEVA CARRERA                                  "); 
   
  textFont(f);
  textAlign(LEFT,CENTER);
      
  final String[] ports = Serial.list();
  printArray(ports);
  println();

 if (ports.length == 0) {
   println("No ports found!");
   //exit();
 }
  
  if (ports.length>0) {
   String portName = ports[0]; //select from device list 
  if (portName !=null) {SPort = new Serial(this, portName, 115200);}
      else {print ("Serial port not detected");};
  }
} 

void label_header(int DX, int DY) {
  textFont(f);
  //textAlign(CENTER, CENTER);
  fill(85, 82, 82);
  rect(DX,DY,BOX_DX,BOX_DY*2);
  fill(0, 255, 0);
  text("RACE TIME  "+nf((time/60000) % 60,2,0)+ ":"+nf((time/1000)%60,2,0)+"."+((time/100) % 10), DX+10,DY+BOX_DY/2);
  fill(255, 255, 0);
  text("POS  DRIVER     LAP   DISTANCE  BATTERY  STATUS ",DX+10,DY+BOX_DY+BOX_DY/2);
}


void label_panel(int DX,int DY) {
  textFont(f2);
  //textAlign(CENTER, CENTER);
  //fill(80, 80, 80);
  //rect(0,0,BOX_DX,BOX_DY*2);  
  //fill(0, 200, 255);
  fill(CSCRL_R,CSCRL_G,CSCRL_B);
  textSize(48);
  text(scrl_text.substring(scrl,scrl+lscrl), DX+10,DY+BOX_DY);
  if ((millis() % 100)>80) scrl++; 
  if (scrl>scrl_text.length()-lscrl) scrl=0;  
}


void label_car(int DX,int DY,int n,int p) {
  textFont(f);
  textSize(24);
  //textAlign(CENTER, CENTER);
  fill(140, 140, 140);  
  rect(DX,DY+p*BOX_DY,BOX_DX,BOX_DY);
  
  noStroke();
  fill(car_col_r[n-1], car_col_g[n-1], car_col_b[n-1]);  
  rect(DX,DY+p*BOX_DY,60+(BOX_DX-60)*(dist[n-1]/(TOT_DIST)),BOX_DY);
  
  stroke(1);
  fill(0, 0, 0);
  textSize(24);
  if (status[n-1]!=0) {text(nom_pos[p],DX+TEXT_POS_X+20,(p*BOX_DY+DY)+BOX_DY/2);};
  textSize(18);
  text(name[n-1],TEXT_NAME_X+DX,(p*BOX_DY+DY)+BOX_DY/2);
  textSize(24);
  text(lap[n-1]+"/"+TOT_LAPS,TEXT_LAP_X+DX,(p*BOX_DY+DY)+BOX_DY/2);
  text(nf(dist[n-1],3,2),TEXT_DIST_X+DX,(p*BOX_DY+DY)+BOX_DY/2);
  text(nf(bat[n-1],3,0),TEXT_BAT_X+DX,(p*BOX_DY+DY)+BOX_DY/2);
  text(nom_status[status[n-1]],TEXT_STAT_X+DX,(p*BOX_DY+DY)+BOX_DY/2);  
}

void lapSort(){
for (int i=0;i<3;i++){
  for (int n=3;n>i;n--){
        if (dist[list_pos[n]-1]>dist[list_pos[i]-1]) {
            int k=list_pos[n];
            list_pos[n]=list_pos[i];
            list_pos[i]=k;            
            };
       };                     
  };      
}

void logos(int DX,int DY){
  image(logo2, DX+BOX_DX-BOX_DY*3,DY, BOX_DY*3, BOX_DY*2);
  image(logo1, DX,DY, BOX_DY*6, BOX_DY*2);
};


void loadTextScroll(String text){
  
scrl_text=text;
while(scrl_text.length()<(lscrl*2)) {scrl_text+=" ";};                                                  
scrl=0;                            
}

void draw() {
  if (status_race==1) time=(millis()-itime);
  background(0);
    
  lapSort();
  label_panel(0,0);
  label_header(0,80); 
  logos(0,0);
  label_car(0,120,list_pos[0],1);
  label_car(0,120,list_pos[1],2);
  label_car(0,120,list_pos[2],3);
  label_car(0,120,list_pos[3],4); 
  
  
  if (mouseX>250 && mouseY>150 && mouseX<350 && mouseY<200) { //if hoover
    fill( 101, 131, 101);
      if (mousePressed) {// and  clicked
      fill( 10, 231, 10);// dark green    
       SPort.write("@");
       delay(10);
       //SPort.write("r");
       //delay(10);
       //SPort.write("4");
       //delay(10);
       //SPort.write(10);
       //delay(10);       
       //SPort.write("~");
    }   
   rect(250, 150, 100, 50);  
   }
 }

void serialEvent(Serial myPort) {
  
  int inByte = SPort.read();  
  
    if (inByte!=10) bstr=bstr+char(inByte);   
    
    if (inByte==10) { //println(bstr.length());
                       if (bstr.length()>0) {println(bstr);
                                             if (bstr.equals("R3")) {itime=millis(); 
                                                                     loadTextScroll(stext[0]); 
                                                                     status_race=1; 
                                                                     CSCRL_R=0;
                                                                     CSCRL_G=200;
                                                                     CSCRL_B=255;  
                                                                     };
                                             if (bstr.equals("R5")) {itime=millis(); 
                                                                     loadTextScroll(stext[2]); 
                                                                     status_race=1;  
                                                                     CSCRL_R=250;
                                                                     CSCRL_G=200;
                                                                     CSCRL_B=0;  
                                                                     };
                                                                     
                                             if (bstr.charAt(0)=='w'){int wp=int(bstr.substring(1,2))-1;
                                                                      loadTextScroll("                 WIN PLAYER "+name[wp]+" !!!");                                                                       
                                                                      status_race=2; 
                                                                      dist[wp]=TOT_LAPS*LEN_TRACK;
                                                                      CSCRL_R=car_col_r[wp];
                                                                      CSCRL_G=car_col_g[wp];
                                                                      CSCRL_B=car_col_b[wp];                                                
                                                                      
                                                                     }; 
                                                                     
                                             if (bstr.charAt(0)=='p'){
                                                      for(int n=0;n<4;n++){
                                                        if (bstr.charAt(1)==('1'+n) ) {ncar1=int(bstr.substring(bstr.indexOf(",")+1,bstr.indexOf(",",bstr.indexOf(",")+1)));
                                                                                       bat[n]=int(bstr.substring(bstr.indexOf(",",bstr.indexOf(",")+1)+1,bstr.length()));                                                                                      
                                                                                       if (bstr.charAt(2)=='M') {track1=1;
                                                                                                                 lap1=int(bstr.substring(bstr.indexOf("M")+1,bstr.indexOf(","))); 
                                                                                                                 lap[n]=lap1;                                                                                                                
                                                                                                                 dist[n]=(lap[n]-1)*LEN_TRACK+(float)ncar1*(LEN_TRACK/100.0);
                                                                                                                 status[n]=1;
                                                                                                                 }
                                                                                       else if (bstr.charAt(2)=='B') {track1=2;
                                                                                                                      lap1=int(bstr.substring(bstr.indexOf("B")+1,bstr.indexOf(",")));
                                                                                                                       if (ncar1<25) status[n]=2;
                                                                                                                       else if (ncar1<75) status[n]=4;
                                                                                                                            else if (ncar1>=75) status[n]=3;                                                                                                                                                                                                                                                      
                                                                                                                       
                                                                                                                      }; 
                                                                                      };
                                                     };                               
                                                                                                      
                                                  
                                                   
                                            }
                      bstr="";
                    };
                    
       }
   
  }
   
  
void mouseReleased() {
  clicked = !clicked;
}  
